// Теоретичні питання

// 1. За допомогою createElement(''), потім за допомогою document.body звертаємось 
// до сторінки, використовуючи append, prepend, before чи after 
// (в залежності від того де ми хочемо його розташувати).

// 2. Перший параметр insertAdjacentHTML вказує на місце розташування вставленого контенту
// по відношенню до елемента.

// Afterbegin початок елементу;
// Beforebegin до самого елементу;
// Afterend після елементу;
// Beforeend цінець елементу.

// 3. Використовуємо метод remove. Пишемо element.remove().

// Завдання

function createList(list, parent){
    list = ["1", "2", "3", "sea", "user", 23, null, 23123];
    let div = document.createElement('div');
    parent = document.createElement('ul');
    div.append(parent);
    parent.innerHTML = list.map(element => `<li>${element}</li>`).join(' ');
    document.body.append(parent);
}

console.log(createList());


